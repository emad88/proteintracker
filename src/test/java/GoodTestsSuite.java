package test.java;

import main.java.BadTestsCetegory;
import main.java.GoodTestsCategory;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.java.HelloJUnitTest;
import test.java.TrackingServiceTest;

/**
 * Created by nikkhouy on 29.2.2016.
 */
@RunWith(Categories.class)
@Categories.IncludeCategory(GoodTestsCategory.class)
@Categories.ExcludeCategory(BadTestsCetegory.class)
@Suite.SuiteClasses(value = {
        TrackingServiceTest.class,
        HelloJUnitTest.class
})
public class GoodTestsSuite {
}
