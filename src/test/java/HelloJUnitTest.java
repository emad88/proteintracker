package test.java;

import main.java.TrackingService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;



/**
 * Created by nikkhouy on 29.2.2016.
 */
public class HelloJUnitTest {

    private TrackingService service;

    @Before
    public void setUp(){
        service = new TrackingService();
    }

    @Test
    public void NewTrackingServiceTotalIsZero() {
        assertEquals("tracking service total was not zero", 0, service.getTotal());
    }

    @Test
    public void WhenAddingProteinTotalIncreasesByThatAmount(){
        service.addProtein(10);
        assertEquals("protein amount was not correct", 10, service.getTotal());
    }


}
