package test.java;

import main.java.BadTestsCetegory;
import main.java.GoodTestsCategory;
import main.java.InvalidGoalException;
import main.java.TrackingService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by emadnikkhouy on 28/02/16.
 */
public class TrackingServiceTest {

    private TrackingService service;

    @Before
    public void setUp(){
        service = new TrackingService();
    }

    @Test
    //@Category(GoodTestsCategory.class)
    public void NewTrackingServiceTotalIsZero() {
        assertEquals("tracking service total was not zero", 0, service.getTotal());
    }

    @Test
    @Category({GoodTestsCategory.class, BadTestsCetegory.class})
    public void WhenAddingProteinTotalIncreasesByThatAmount(){
        service.addProtein(10);
        assertEquals("protein amount was not correct", 10, service.getTotal());
        assertThat(service.getTotal(), is(10));
        assertThat(service.getTotal(), allOf(is(10), instanceOf(Integer.class)));
    }

    @Test
    @Category(GoodTestsCategory.class)
    public void WhenRemovingProteinTotalRemainsZero(){
        service.removeProtein(5);
        assertEquals("Total suppose to be zero after removing protein",0, service.getTotal());
    }
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test //(expected = InvalidGoalException.class)
    public void WhenGoalIsSetToLessThanZeroExceptionIsThrown() throws InvalidGoalException {
        thrown.expect(InvalidGoalException.class);
        thrown.expectMessage("Goal was less than zero!");
        thrown.expectMessage(containsString("Goal"));
        service.setGoal(-5);
    }

    @Rule
    public Timeout timeout = new Timeout(20);  // applied

    @Test // (timeout = 100)
    @Ignore
    public void BadTest(){
        for(int i=0; i<100000000; i++)
            service.addProtein(i);
    }
}
