package test.java;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.java.TrackingServiceTest;

/**
 * Created by nikkhouy on 29.2.2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        TrackingServiceTest.class,
        HelloJUnitTest.class
})
public class ProteinTrackerSuite {
}
