package main.java;

/**
 * Created by emadnikkhouy on 28/02/16.
 */
public class InvalidGoalException extends Exception {


    public InvalidGoalException(String message){
        super(message);
    }
}
